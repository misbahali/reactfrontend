import React from "react";
import TodoForm from "./TodoForm";
import Todo from "./Todo";
import axios from 'axios';


export default class TodoList extends React.Component {
  constructor(){
    super();
    this.state={
      todos:[]
    }
  }

  componentDidMount(){
    //const tok = 'misbah@admin.com:admin123'

    /*
    axios.get('https://jsonplaceholder.typicode.com/todos/')
    .then(response=>{
      this.setState({todos:response.data});
    });*/

    
    axios.get('http://127.0.0.1:8000/api/todo/', {headers:{'Authorization': 'Basic bWlzYmFoQGFkbWluLmNvbTphZG1pbjEyMw==' }})
    .then(response=>{
      this.setState({todos:response.data});
    });


    
  }

  state = {
    todos: [],
    todoToShow: "all",
    toggleAllComplete: true
  };

  getTodo = todo => {
    axios.get('http://127.0.0.1:8000/api/todo/', {headers:{'Authorization': 'Basic bWlzYmFoQGFkbWluLmNvbTphZG1pbjEyMw==' }})
    .then(response=>{
      this.setState({todos:response.data});
    });
  }

  addTodo = todo => {
  var tempDate = new Date();
  var date = tempDate.getFullYear() + '-' + (tempDate.getMonth()+1) + '-' + tempDate.getDate() +' '+ tempDate.getHours()+':'+ tempDate.getMinutes()+':'+ tempDate.getSeconds();
  
  const params = new URLSearchParams();
  params.append('user_id', '2');
  params.append('title', todo.title);
  params.append('completed', '0');
  params.append('date_created', date);
  params.append('description', "some data");

    axios.post('http://127.0.0.1:8000/api/todo/', params, {
        headers: {
            'Authorization': 'Basic bWlzYmFoQGFkbWluLmNvbTphZG1pbjEyMw==',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
})
.then(response => { 
  console.log(response)
  this.getTodo();
})
.catch(error => {
    console.log(error.response)
});
  };

  toggleComplete = id => {
    const putParamsComplete = new URLSearchParams();
    putParamsComplete.append('completed', '1');

    const putParamsUncomplete = new URLSearchParams();
    putParamsUncomplete.append('completed', '0');

    this.setState(state => ({
      todos: state.todos.map(todo => {
        if (todo.id === id) {
          // suppose to update
          if(!todo.complete){
            console.log(todo.id);
            console.log("complete");
            axios.put('http://127.0.0.1:8000/api/todo/'+todo.id, putParamsComplete, {
        headers: {
            'Authorization': 'Basic bWlzYmFoQGFkbWluLmNvbTphZG1pbjEyMw==',
            'Content-Type': 'application/x-www-form-urlencoded'
        }})
          } else if(todo.complete){
            console.log(todo.id);
            console.log("uncomplete");
            axios.put('http://127.0.0.1:8000/api/todo/'+todo.id, putParamsUncomplete, {
        headers: {
            'Authorization': 'Basic bWlzYmFoQGFkbWluLmNvbTphZG1pbjEyMw==',
            'Content-Type': 'application/x-www-form-urlencoded'
        }})
          }
          return {
            ...todo,
            complete: !todo.complete
          };
        } else {
          return todo;
        }
      })
    }));
  };

  updateTodoToShow = s => {
    this.setState({
      todoToShow: s
    });
  };

  handleDeleteTodo = id => {
   console.log(id);
    this.setState(state => ({
      todos: state.todos.filter(todo => todo.id !== id)
    }));
    axios.delete('http://127.0.0.1:8000/api/todo/'+id, {headers:{'Authorization': 'Basic bWlzYmFoQGFkbWluLmNvbTphZG1pbjEyMw==' }});
  };

  removeAllTodosThatAreComplete = () => {
    this.setState(state => ({
      todos: state.todos.filter(todo => !todo.complete)
    }));
  };

  render() {
   return (
    <div>
       <TodoForm onSubmit={this.addTodo} />
      <div>
        todos left: {this.state.todos.filter(todo => !todo.complete).length}
      </div>
      <div>
      <button onClick={() => this.updateTodoToShow("all")}>all</button>
      <button onClick={() => this.updateTodoToShow("active")}>
        active
      </button>
      <button onClick={() => this.updateTodoToShow("complete")}>
        complete
      </button>
    </div>

    {this.state.todos.some(todo => todo.complete) ? (
      <div>
        <button onClick={this.removeAllTodosThatAreComplete}>
          remove all complete todos
        </button>
      </div>
    ) : null}
    <div>
      <button
        onClick={() =>
          this.setState(state => ({
            todos: state.todos.map(todo => ({
              ...todo,
              complete: state.toggleAllComplete
            })),
            toggleAllComplete: !state.toggleAllComplete
          }))
        }
      >
        toggle all complete: {`${this.state.toggleAllComplete}`}
      </button>
    </div>
      {
        this.state.todos.map(todo=>{
          return (
            <div>
            <Todo
            key={todo.id}
            toggleComplete={() => this.toggleComplete(todo.id)}
            onDelete={() => this.handleDeleteTodo(todo.id)}
            todo={todo}
            />
            </div>
          
          )
        })
      }   
     </div>
   );
  }
}

