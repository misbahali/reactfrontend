# README #

### What is this repository for? ###

* Quick summary:
	This is the FrontEnd for the LaravelBackEnd API - It consumes the API, coded with React
* Version:
	1
* Notes:
	Todo's HAVE to be more than 4 letters to post properly because I put that restriction in the backend api because why not.

### How do I get set up? ###

* Summary:
	Download the project
	Run "npm start in the root folder"
	Make sure you have the backend configured and running
	Make sure you run "npm install" to install all dependencies first
	
* Configuration:
	it doesn't matter since the user is hard-coded in the Todolist.js file but;
		username: misbah@admin.com
		password: admin123
		
* Dependencies:
	React
	Axios

### Who do I talk to? ###

*	Muhammad Misbah Ali - muhammadmisbahali@gmail.com